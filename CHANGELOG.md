19.12.2015 - Erstellung der README.md (Dustin Weber), CONTRUBUTING.md(Manuel Gerzen) und CHANGELOG.md (Dustin Weber, Manuel Gerzen) Dateien.
02.01.2016 - Mit GitHub anstatt GitThm auseinander gesetzt. Kein umstieg! GitThm wird weiterhin verwendet.(Dustin Weber, Manuel Gerzen)
03.01.2016 - Erstellung For-Schleife.(Dustin Weber) Fragen-Grundgerüst(Dustin Weber, Manuel Gerzen).
04.01.2016 - Fertigstellung der ersten Version des Programms! (Manuel Gerzen, Dustin Weber)
06.01.2016 - Erstellung der Neustart und Beenden Funktion. Version 1.0 fertig! Ausarbeitung / Verbesserung des Programmes geplant! (Dustin Weber)
			 break; in der "Beenden" Funktion durch return(0); ersetzt.
07.01.2016 - Fragen Array erstellt (Manuel Gerzen) Fragen in Ausgaben eingesetzt (Dustin Weber).
08.01.2016 - Array mit ("Bitte geben Sie 1,2,3, oder 4 ein") erstellt. Unnötige If Abfrage / For Schleife entfernt (Dustin Weber, Manuel Gerzen)