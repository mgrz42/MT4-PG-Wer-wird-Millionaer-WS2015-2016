// WWMMGDW.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <conio.h>
#include <windows.h>


int main()
{
	int Antwort, // Eingabe Benutzer f�r Antwort auf die Frage
		neustart,
		Anzahlfragen = 15, // Anzahlfragen f�r Schleifendruchl�ufe!
		loesung = 0; // Setzen der richtigen L�sung

	char* Fragen[15] = {
		//Frage & Anworten 0
		"50 Euro | Frage1: Wobei handelt es sich um ein Notsignal im internationalen Funkverkehr?\n"
		"	1: Mayday\n" "	2: Down Town\n" "	3: Jetset\n" "	4: Flower Power\n",
		//Frage & Anworten 1
		"\n100 Euro | Frage2: Was erzeugt Laerm und Abgase?\n"
		"	1: Pkw-Petting\n" "	2: Cabriosex\n" "	3: Autoverkehr\n" "	4: Omnibussi\n",
		//Frage & Anworten 2
		"\n200 Euro | Frage3: Wer bewegt sich paddelnd vorwaerts?\n"
		"	1: Solgerda\n" "	2: Wililse\n" "	3: Muslisa\n" "	4: Kanute\n",
		//Frage & Anworten 3
		"\n300 Euro | Frage4: Schreibt man seiner Liebsten ein Gedicht, aeussert man seine Zuneigung ...?\n"
		"	1: ab Artig\n" "	2: bi Zarr\n" "	3: gro Tesk\n" "	4: per Vers\n",
		//Frage & Anworten 4
		"\n500 Euro | Frage5: Leidet man unter Alopezie, werden die Haare ...?\n"
		"	1: zacherl\n" "	2: maelzer\n" "	3: lichter\n" "	4: lafer\n",
		//Frage & Anworten 5
		"\n1.000 Euro | Frage6: Welches Verb steht im Duden ?\n"
		"	1: hesseln\n" "	2: thueringeln\n" "	3: saechseln\n" "	4: vorpoemmeln\n",
		//Frage & Anworten 6
		"\n2.000 Euro | Frage7: Womit verdienen auch heute noch viele Leute ihren Lebensunterhalt?\n"
		"	1: Schwammtauchen\n" "	2: Lappenfischen\n" "	3: Besenschnorcheln\n" "	4: Buerstenangeln\n",
		//Frage & Anworten 7
		"\n4.000 Euro | Frage8: Welches Land verraet durch seinen Namen, an welchem Breitengrad es liegt?\n"
		"	1: Brasilien\n" "	2: Ecuador\n" "	3: Paraguay\n" "	4: Venezuela\n",
		//Frage & Anworten 8
		"\n8.000 Euro | Frage9: Sigmund Jaehn war vor 30 Jahren der erste Deutsche ...?\n"
		"	1: im Weltall\n" "	2: auf dem Papststuhl\n" "	3: im Europaparlament\n" "	4: als Direktor der Met\n",
		//Frage & Anworten 9
		"\n16.000 Euro | Frage10: Wie viele Spiele muss die DFB-Elf bei der Fussball-EM 2008 mindestens gewinnen, um den Titel zu holen?\n"
		"	1: alle\n" "	2: 1\n" "	3: 3\n" "	4: 5\n",
		//Frage & Anworten 10
		"\n32.000 Euro | Frage11: Wer machte sein Abitur mit 1,0?\n"
		"	1: Angela Merkel\n" "	2: Juergen Klinsmann\n" "	3: Harald Schmidt\n" "	4: Verona Pooth\n",
		//Frage & Anworten 11
		"\n64.000 Euro | Frage12: Woran machte sich Opa zu schaffen, wenn er am Perpendikel herumfummelte?\n"
		"	1: Moped\n" "	2: Grammofon\n" "	3: Standuhr\n" "	4: Oma\n",
		//Frage & Anworten 12
		"\n125.000 Euro | Frage13: Was ist auf US-amerikanischen Dollarnoten abgebildet?\n"
		"	1: Pyramide & Auge\n" "	2: Obelisk & Mund\n" "	3: Sphinx & Ohr\n" "	4: Pharao & Nase\n",
		//Frage & Anworten 13
		"\n500.000 Euro | Frage14: Wie viele Nummer-1-Hits hatte Elvis Presley zu Lebzeiten in Deutschland?\n"
		"	1: keinen\n" "	2: einen\n" "	3: 5\n" "	4: 17\n",
		//Frage & Anworten 14
		"\n1.000.000 Euro | Frage15: Das Nagel-Schreckenberg-Modell liefert eine Erklaerung f�r die Entstehung von ...?\n"
		"	1: Sandwuesten\n" "	2: Verkehrsstatus\n" "	3: Grippewellen\n" "	4: Boersencrashs\n" };


	//schleife f�r die Fragen 1-15
	//Fragenzaehler als Variable f�r die Ausgabe der Arrays "Fragen"
	//Anzahlfragen f�r die Maximale anzhal an Schleifendurchl�ufe
	for (int Fragenzaehler = 0; Fragenzaehler < Anzahlfragen; Fragenzaehler++) {

		printf("%s", Fragen[Fragenzaehler]);

		if (Fragenzaehler == 0) {
			loesung = 1;
		}
		if (Fragenzaehler == 1) {
			loesung = 3;
		}
		if (Fragenzaehler == 2) {
			loesung = 4;
		}
		if (Fragenzaehler == 3) {
			loesung = 4;
		}
		if (Fragenzaehler == 4) {
			loesung = 3;
		}
		if (Fragenzaehler == 5) {
			loesung = 3;
		}
		if (Fragenzaehler == 6) {
			loesung = 1;
		}
		if (Fragenzaehler == 7) {
			loesung = 2;
		}
		if (Fragenzaehler == 8) {
			loesung = 1;
		}
		if (Fragenzaehler == 9) {
			loesung = 3;
		}
		if (Fragenzaehler == 10) {
			loesung = 1;
		}
		if (Fragenzaehler == 11) {
			loesung = 3;
		}
		if (Fragenzaehler == 12) {
			loesung = 1;
		}
		if (Fragenzaehler == 13) {
			loesung = 2;
		}
		if (Fragenzaehler == 14) {
			loesung = 2;
		}

		printf("Bitte geben Sie 1, 2, 3 oder 4 ein!\n");

		scanf_s("%d", &Antwort);

		if (Antwort == loesung) {
			printf("Richtig! \n");
		}
		else
		{
			printf("Falsch! Neustart bitte 1 eingeben. Fuer beenden 2\n");
			scanf_s("%d", &neustart);
			if (neustart == 1) {
				main();
			}
			else if (neustart == 2) {
				exit(0);
			}
		}
	}

	printf("\n\nDu bist Millionaer!\n\nNeustart bitte 1 eingeben. Fuer beenden 2\n");
	scanf_s("%d", &neustart);
	if (neustart == 1) {
		main();
	}
	else if (neustart == 2) {
		exit(0);
	}
}
